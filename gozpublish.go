package main

import (
	"context"
	"encoding/json"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/streadway/amqp"

	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	gozilla "gitlab.inria.fr/osallou/gozilla-lib"

	elasticsearch "github.com/elastic/go-elasticsearch"
)

// Version of server
var Version string

type favContextKey string

// PublishHandler manages publish requests
type PublishHandler struct {
	Goz gozilla.GozContext
}

func (p PublishHandler) publish(pack gozilla.PackageVersion) {
	log.Debug().Str("package", pack.Package).Msg("publish package")
	_, pubErr := pack.SetMetaFiles(p.Goz)
	if pubErr != nil {
		log.Error().Err(pubErr).Msg("failed to publish package")
	} else {
		now := time.Now()
		ts := now.Unix()
		pack.Published = true
		pack.PublishedTime = ts
		pack.Save(p.Goz)
		log.Debug().Msgf("package published %+v", pack)
	}
}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") == "1" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		panic(storageErr)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating the client: %s", err)
		os.Exit(1)
	}

	amqpHandler, amqpErr := gozilla.NewAmqpHandler(config)
	if amqpErr != nil {
		log.Error().Err(amqpErr).Msg("rabbitmq error")
		os.Exit(1)
	}
	defer amqpHandler.Conn.Close()

	msgs, consumeErr := amqpHandler.Ch.Consume(
		amqpHandler.PublishQueue, // queue
		"publish",                // consumer
		false,                    // auto-ack
		false,                    // exclusive
		false,                    // no-local
		false,                    // no-wait
		nil,                      // args
	)

	if consumeErr != nil {
		log.Error().Err(consumeErr).Msg("failed to get messages")
		panic("failed to get messages")
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM)

	go func(connection *amqp.Connection, channel *amqp.Channel) {
		sig := <-sigs
		channel.Close()
		connection.Close()
		log.Warn().Msgf("Closing AMQP channel and connection after signal %s", sig.String())
		log.Warn().Msg("Ready for shutdown")
	}(amqpHandler.Conn, amqpHandler.Ch)

	gozCtx := gozilla.GozContext{
		Mongo:       mongoClient,
		Config:      config,
		Storage:     storage,
		AmqpHandler: amqpHandler,
		Elastic:     es,
	}

	pHandler := PublishHandler{
		Goz: gozCtx,
	}

	forever := make(chan bool)

	go func(p PublishHandler) {
		log.Debug().Msgf("listen for messages on %s", amqpHandler.PublishQueue)
		for d := range msgs {
			var pack gozilla.PackageVersion
			err := json.Unmarshal(d.Body, &pack)
			if err == nil {
				p.publish(pack)

			} else {
				log.Error().Str("body", string(d.Body)).Msg("failed to decode message")
			}
			d.Ack(true)
		}
	}(pHandler)

	<-forever

}
