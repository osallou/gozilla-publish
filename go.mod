module gitlab.inria.fr/osallou/gozilla-publish

go 1.13

require (
	github.com/armon/go-metrics v0.3.3 // indirect
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/gophercloud/gophercloud v0.9.0 // indirect
	github.com/hashicorp/consul/api v1.4.0 // indirect
	github.com/hashicorp/go-hclog v0.12.2 // indirect
	github.com/hashicorp/go-immutable-radix v1.2.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/hashicorp/serf v0.9.0 // indirect
	github.com/klauspost/compress v1.10.4 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/zerolog v1.18.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/tidwall/pretty v1.0.1 // indirect
	gitlab.inria.fr/osallou/gozilla-lib v0.0.0-20200410100459-7f7c510e9bc2
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
)
